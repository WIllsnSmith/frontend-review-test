import api from '../../api/products.js';
import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    cart: [],
    currency: 'VGTB',
    products: [],
  },
  mutations: {
    addToCart(state, { amount, price, title, productId }) {
      state.cart.push({ amount, price, title, productId })
    },
    updateProducts(state, products) {
      state.products = products;
    }
  },
  actions: {
    async refreshProducts({ commit }, data) {
      let products = await api.getProductsList();
      commit('updateProducts', products);
    },
  },
  modules: {
  }
})
